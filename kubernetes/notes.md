# P1: k3s dans deux machines avec vagrant:

L'une des limites de Vagrant est que le script qui permet de provisionner une
vm ne peut pas être réutilisé sur plusieurs machines. Cf issue #10976
C'est un problème, parce que les téléchargements sont multipliés par le nombre
de machines que l'on
souhaite avoir.

```
config.vm.provision "shell" do |sh
	sh.privileged = true
	sh.inline = " yum update -y && \
		yum install -y net-tools && \
		curl -sfL https://get.k3s.io | sh -"
end
```

Une solution possible serait de déployer une box avec:
- la dernière version de centos
- net-tools
- k3s

Pour cela il faut lancer cette commande une fois la box centos provisionnée;
à la main ou avec un autre moyen.

```
$ vagrant package --base my-virtual-machine
```

il faut ensuite suivre ce tutoriel pour avoir une box dans le cloud.
https://www.vagrantup.com/vagrant-cloud/boxes/create

```
  config.vm.box = "mehdee/k3s_one"
  config.vm.box_version = "0.1.1"
```

Une fois la box déployée, la grosse difficulté a été de comprendre une option
pas très bien documentée de k3s. Pour vagrant (qui utilise un vxlan, cni pour
pouvoir regrouper le cluster) eth0 est forcément l'adresse externe à laquelle
il va chercher à bind (côté serveur), si on ne spécifie pas le node-ip pour le
serveur, l'agent n'arrive pas à le retrouver dans le réseau. Et ceci, car par
défaut il bind sur le vxlan.

https://github.com/k3s-io/k3s/issues/1267 (and other ones since the helper
script only helps if things go well...)

Le prochain challenge pour cette partie est de gérer correctement les secrets
ainsi que de changer l'ip de ..56. à ..42. comme demandé dans le sujet.

commu: https://slack.rancher.io/
and github issues

#P3: argo cd for vlc.js with fixed libvlc versions

La première étape consiste à créer deux versions d'une image docker, qui vont
télecharger deux versions différentes de libvlc, une stable et une autre "tip
of tree", le but de cette partie va être de pouvoir servir deux versions
de vlc.js en fonction du tag demandé.

L'image docker doit avoir de quoi lancer un serveur qui va déployer une page
web avec webassembly, en respectant la policy COOP/COEP.

TODO:

(

Elle doit aussi lancer le script `create_main.sh` après avoir comparé avec
un hash et décompressé les libraries statiques de libvlc.wasm
(avec les contribs / dépendances).

)


L'archive doit avoir la même structure que les sources de vlc:

- contrib dans vlc/contrib/contrib-emscripten
- librairies dans build-emscripten

L'image docker sera basée sur nginx.

```
FROM nginx:1.23.1-alpine

COPY confs/nginx.conf /etc/nginx/nginx.conf
COPY app /usr/share/nginx/html
```

Un bug que je ne suis pas arrivé à régler pour nginx, mais avec un dockerfile
qui utilise python, ça fonctionne.

Le dockerfile est dans le dossier p3, une fois ce dernier valide, il faut le
pousser dans un registre dockerhub.

```
# commandes pour push le conteneur dans dockerhub
docker login
docker commit app-vX <container-name>:<version>
docker tag <container name>:<version> <username>/<container name>:<version>
docker push <username>/<container name>:<version>
```

Les container sont hébergés ici :

https://hub.docker.com/r/msabwat/playground

Il ne faut pas oublier de publier le port quand on lance l'un des containers.

Une fois cette étape faite, il suffit d'installer k3d et de l'utiliser pour
créer un cluster avec un node et 2 namespaces comme demandé dans le sujet.

Un namespace pour argo cd, qui est un outil qui va nous aider à ajouter un
"hook" sur le repo git de l'image docker, pour que quand cette dernière
change de tag que le déploiement se fasse automatiquement.

Un autre namespace pour l'application vlc.js.


