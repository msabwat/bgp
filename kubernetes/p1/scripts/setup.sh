# this script is not run because it does not make sense to download 500M and
# unpack it twice

yum update -y && yum install -y net-tools && curl -sfL https://get.k3s.io | sh -
# install guest additions in the vb machine
# tutorial here:
# https://www.tecmint.com/install-virtualbox-guest-additions-in-centos-rhel-fedora/

# The issue here is that the vagrant plugin does not understand the additions
# are already there, and reinstalls them every time. To workaround this,
# auto updates are just disabled

# It was added to https://app.vagrantup.com/mehdee/boxes/k3s_one
# Vagrant directly pulls it, with the configuration files in ../confs
# Commands to setup the box in the Vagrant cloud, are detailed in notes.md
