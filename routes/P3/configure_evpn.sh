#!/bin/sh
if [ $EVPN_DEVICE = leaf ]; then
	ip link add name br0 type bridge
	ip link set br0 up
	ip link add vxlan10 type vxlan id 10 dstport 4789
	ip link set vxlan10 up
	brctl addif br0 vxlan10
	brctl addif br0 eth0

	cp /etc/frr/frr_leaf.conf /etc/frr/frr.conf
else
	cp /etc/frr/frr_rr.conf /etc/frr/frr.conf
fi

/usr/lib/frr/docker-start &
/bin/sh
