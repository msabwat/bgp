#!/bin/sh
if [ $VXLAN_CONF = static ]; then
	ip link add vxlan10 type vxlan id 10 remote $REMOTE_VXLAN_IP dstport 4789 dev eth0
else
	ip link add vxlan10 type vxlan id 10 group $GROUP_VXLAN_IP dstport 4789 dev eth0
fi
ip link set vxlan10 up
ip link add name br0 type bridge
ip link set br0 up
ip link set vxlan10 master br0
ip link set eth1 master br0

/bin/sh
