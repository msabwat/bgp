Installer et configurer GNS3 et docker dans une VM [ok]
doc:
http://openmaniak.com/fr/quagga.php
https://vincent.bernat.ch/en/blog/2017-vxlan-bgp-evpn#introduction-to-bgp-evpn

Instructions sur arch:
gns3 client
gns3 server
dynamips (pour les émulateurs de hw)
ubridge - ne pas oublier de modprobe tap
docker

Gros défaut de mon setup: je ne peux pas lancer des images docker directement dans gns3, je dois passer par dockerhub
(update): en téléchargeant et paramétrant correctement la vm gns3, j'arrive à ajouter des containers dans les templates.

TODO:
1. faire un setup simple dans GNS avec un routeur (image docker) et un host (un busybox):
quagga - services (bgp, is-is, ospf)
2. setup un vxlan (topo dans le sujet) - static et dynamic multicast
3. setup un evpn 

# Définitions

BGP est un protocole de routage dynamique.

Comment faire transiter un paquet d'une interface réseau à une autre, de la meilleure façon? Le routeur est un équipement réseau qui permet de répondre à cette question.
Il sélectionne des chemins dans un réseau pour acheminer les données d'un expéditeur à un ou plusieurs destinataires.

Passerelle:
Quand le routeur se trouve entre deux réseaux dépendant d'autorités différentes, dans ce cas une conversion entre protocoles est effectuée.

Table de routage:
Liste des réseaux connus, chaque réseau peut être associé à un ou plusieurs routeurs. Elle contient trois types de routes:

- réseaux directement connectés (ETH)
Topo, deux machines, deux routeurs. On ne peut pas envoyer de paquet d'une machine à l'autre, mais on peut l'envoyer au routeur, qui va l'envoyer à l'autre routeur, qui va l'envoyer à la machine.
- routes statiques
Pour le même exemple, il se peut que nous ne controlions que la configuration d'un seul des deux routeurs. Dans ce cas pour envoyer un paquet d'une machine à l'autre, il faut ajouter une route pour dire au routeur courant que le prochain sera une passerelle, et qu'on veut qu'elle nous mène (chemin) à la machine souhaitée. 
- routes dynamiques
Si au lieu d'avoir une route possible, on en a plusieurs. Quelle est la plus rapide? Y a t il d'autres critères pour choisir une route? Si une route n'est plus valide. (Link fails).
Un routeur communique ses routes à un autre à travers un protocol de routage. Il met aussi à jour sa table de routage pour connaitre la route à prendre s'il veut joindre un réseau. (Topo: R1 => R2/R3 => R4) et d'un coup R2 n'arrive plus à joindre R4. R2 sait qu'il peut joindre R4 à travers R1.

Comment le routeur trouve la meilleure route?
Il faut que le routeur aie une connaissance de la topologie du réseau, celle ci est transmise par des protocoles de routage qui spécifient la façon dont les informations sont échangées. Avec cette connaissance, des algorithmes de routage efficaces sont implémentés.

netmasks: https://www.pawprint.net/designresources/netmask-converter.php

# Partie 1: Un routeur avec des services et une machine.

FRR => le gagnant du jour.
Quagga - plus mis à jour sur debian (mauvais signe), pas évident à paramétrer sur arch dans un docker sans systemd
BIRD, ne supporte pas IS-IS
Freertr a besoin de java, a besoin d'être lancé en root, a des fichiers de configuration qui ont l'air difficiles à paramétrer rapidement.

Tout ce qui est dit après est basé sur la documentation dans docs.frrouting.org

_msabwat-1_host:
- créer le fichier /etc/frr/vtysh.conf
La méthode de configuration sera: (tout en un au lieu d'un fichier par daemon)
service integrated-vtysh-config
Les fichiers de configuration par service sont quand même créés.

- faire un script à copier dans le dockerfile pour (à coup de sed), changer les entrées de /etc/frr/daemons
Pour un service, par exemple, cette ligne va activer le service bgp

sed -i '/bgpd=no/s/.*/bgpd=yes/' /etc/frr/daemons

dans la vm gns3:

docker build -f msabwat-1_host -t <image name in gns3>

_msabwat-2:

Une image de base pour une machine avec busybox. (alpine)
dans la vm gns3:

docker pull alpine

Dans gns3, on peut créer des templates à partir de ces deux images.
Le hostname étant relié à un hostname docker, le nom ne peut pas contenir d'underscore comme pour la machine. (RFC1123 et code source de gns3)

Pour respecter le sujet, on se contente de renommer l'item pour que, par effet de cascade il renomme le hostname de l'image docker aussi. Je ne suis pas sûr que
ce comportement soit accepté et qu'il ne causerait pas des problèmes.

On ne peut pas changer le hostname avec un clic droit sur l'item car l'ui nous en empêche, dans le code une référence à la rfc est faite.

Le routeur a les services suivants:

- watchfrr:
Ce service de FR-Routing permet de gérer les redémarrages de services si besoin. On peut lui demander d'ignorer certains services.

- zebrad:
Ce service a pour objectif de gérer les tables de routage des interfaces réseaux présentes sur une machine. Aussi, il peut les grouper (filtrer ?) pour les communiquer à d'autres services qui en ont besoin. 

- staticd:
Ce daemon, disponible par défaut, permet de gérer des routes statiques. 

- bgpd:
Ce daemon a pour but de gérer le border gateway protocol. C'est un External Gateway Protocol. Il permet de gérer (par l'échange d'informations) la sélection des routes entre les Systèmes Autonomes (Autonomous Systems), avec des outils comme les Views et les Address Family.

- isisd:
Ce daemon a pour but de gérer le intermediate system to intermediate system protocol. C'est un Interior Gateway Protocol. Il a pour but de trouver la meilleure route pour un réseau de qui fait transiter des packets.

La différence entre IGP et EGP est que les IGP vont échanger des informations de gateway à gateway dans un AS, les EGP quand à eux échangent des informations entre AS.

- ospfd:
Ce daemon gère un composant du routage qui consiste à trouver le chemin le plus court pour atteindre un équipement. La différence avec BGP est qu'il est basé sur les liens au lieu de la distance (nombre de routeurs).

# Partie 2: Un réseau VXLAN, en statique puis dynamique multicast.

Un réseau VLAN ou réseau local virtuel, est un réseau logique qui est intéressant quand on souhaite regrouper des flux, les sécuriser ou réduire la taille d'un gros réseau.

Le VXLAN a une composante extensible en plus. Il permet d'addresser des problèmes d'évolutivité (network scaling).

Pour la partie 2 du sujet, il est question de créer un vxlan avec deux routeurs et un switch. Chaque routeur a deux interfaces. Une pour le connecter au switch et une pour un host.

Le but est d'arriver à faire un ping à partir d'un host vers le second, dans un premier temps en statique puis dynamique multicast.

GNS3 nous permet de définir la topologie et grâce au fichier de configuration `interfaces` de définir les addresses ip des différents équipements.

Il manque les interfaces vxlan et bridge, qui sont ajoutées ensuite grâce à un script. Pour chaque routeur une interface (eth1) agit comme une gateway pour les deux hosts. Elles ont la même addresse ip. L'interface eth0 permet d'identifier les routeurs dans le réseau local.

Le vxlan va regrouper ce réseau en un, et un bridge sera créé avec le vxlan et les interfaces eth1 pour que le trafic passe d'une machine à l'autre d'une manière transparente pour les nodes extérieurs.

Pour de larges déploiements vxlan, il y a deux problèmes qu'on cherche à éviter/minimiser:

- pouvoir découvrir de nouveaux endpoints (vtep) - sinon, l'extensibilité est limitée.
- éviter les BUM frames - ce qui pose un problème de performance dans certains cas, car l'envoi de ces messages peut être évité.
Broadcast, Unknown unicast et multicast.

Une solution possible pour le premier challenge est le multicast: au lieu d'ajouter les addresses au vxlan une à une, on crée un groupe. Une solution à l'autre problème sera développée dans la partie suivante.

# Partie 3: BGP EVPN

WIP:

L'idée de cette partie est que dans un réseau de 4 routeurs, un servira à stocker les routes (RR), et trois autres serviront de terminaux d'entrées au réseau VXLAN. Ainsi, lorsqu'un paquet avec une addresse IP inconnue arrive dans un VTEP, au lieu de faire de la découverte de MAC avec des Broadcast, Unicast ou Multicast, le VTEP va interroger le Route Reflector (RR): Si le host demandé a été annoncé, sa route sera connue tout de suite.

Il est demandé dans le sujet de configurer un routeur ospf, qui nous facilitera la tâche pour transmettre les informations de link (layer2) aux voisins immédiats. (neighbors) En plus des routes, qui elles sont échangées entre zebra et bgpd. 

## configuration du route reflector:

Dans un shell vty:

1. Setup des interfaces
commandes interface et "ip address" dans vtysh 

2. Setup bgp
router bgp $AS
neighbor ibgp peer-group
neighbor ibgp remote-as $AS
neighbor ibgp update-source $loopback_ip
bgp listen range 1.0.0.0/29 peer-group $PEER

3. Setup evpn
address-family l2vpn evpn
neighbor ibgp activate
neighbor ibgp route-reflector-client
exit-address-family

4. Setup ospf
router ospf
network 0.0.0.0/0 area 0

Déroulement de la simulation:

## configuration des vtep:

1. Setup des interfaces
comme au dessus
avec "ip ospf area $AREA_ID", le même area id pour tous les vteps

2. Setup bgp
router bgp $AS
neighbor $RR_IP remote-as $AS
neighbor $RR_IP update-source $loopback_ip

3. Setup evpn
address-family l2vpn evpn
neighbor $RR_IP activate
exit-address-family

4. Setup ospf
router ospf

EVPN Route type http://bgphelp.com/2017/05/22/evpn-route-types/


https://france.devoteam.com/paroles-dexperts/mettre-en-place-des-tunnels-vxlan-grace-aux-extensions-bgp-evpn/

https://networkdirection.net/articles/routingandswitching/vxlanoverview/vxlanaddresslearning/

https://ilearnedhowto.wordpress.com/2017/02/16/how-to-create-overlay-networks-using-linux-bridges-and-vxlans/
